#include <stdio.h>

int main(int argc, const char * argv[])
{
    int n = 6c;
    int f = fib (n);
    printf("fibonacci di %d è: %d\n", n, f);

}

int fib (int n)
{
    if (n == 0 || n == 1) return n;

    int prec = 1, post = 1, temp;

    while (n > 0) 
    {
        temp = post;
        post += prec;
        prec = temp;
        n--;
    }  
}
