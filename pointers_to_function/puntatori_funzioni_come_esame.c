#include <stdio.h>

//Per scambiare due valori non ho bisogno del valore ma dell' indirizzo
//Passo per copia l' indirizzo delle variabili

void swap(int *xp, int *yp)
{
	//RICORDA: Usando l' asterisco leggo il contenuto
	int temp = *xp;
	*xp = *yp;
	*yp = temp;
}





void selectionSort (int arr[], int n, int(*comp)(int,int)
{
	int i, j, min_idx;

	for (i = 0; i < n - 1; i++) 
	{
		//trova minimo elemento nel vettore
		for (int j = i +1; j < n; j++ ) 
		{
			if (comp(arr[j], arr[min_idx]))
				min_idx = j;			
		}
		//scambio con l' elemento della iesima iterazione
		swap(&arr[min_idx], &arr[i]);	
	}
}


int minore (int a, int b) {return (a < b)? 1:0;}

int maggiore(int a, int b) {return (a < b)? 0:1;}



int main (int argc, const char * argv[]) 
{
	int v = {3,1,5,2,7,4,6,9,8};
	int (*ptrfun)(int, int);

	ptrfun= minore; // o ptrfun = maggiore;

	selectionSort(v,9,ptrfun);
	
	int i;
	for( i = 0;i < 9; i++ )
	{
		printf("v[%d] = %d\n", i, v[i]);	
	}
}




//VARIANTE


int main (int argc, const char * argv[]) 
{
	int v = {3,1,5,2,7,4,6,9,8};
	int (*ptrfun)(int, int);
	int *pt;

	ptrfun= minore; // o ptrfun = maggiore;
	pt = (int*) ptrfun //ho fatto un cast

//per la linea sotto ho un errore in esecuzione! Perchè si ha un area di codice proetta.
// Ciò e dipendente dal sistema operativo
	*pt = 23; 

	printf("indirizzo di memoria di minore => %p\n", minore);//vengono letti i byte come il valore di un intero
	//Prova a stampare quelli delle altre funzioni e vedere la vicinanza
	selectionSort(v,9,ptrfun);
	
	int i;
	for( i = 0;i < 9; i++ )
	{
		printf("v[%d] = %d\n", i, v[i]);	
	}
}














