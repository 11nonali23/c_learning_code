

//gli argomenti del main:

// 1 quanti argomenti sono da inserire sulla linea di      comando
//  2 è un vettore di stringhe 

//così facendo il main può elaborare i dati sulla linea di comando

#include <stdio.h>

int main(int argc, const char * argv[])
{
//     printf("Hello World%d\n"); //facendo cosi esce:
//     //Hello World-1345744712 dove viene letto un numero di byte in un posto di memoria "strano" interpretadolo come un intero in quanto non ho specificato interi
//     return 0;
    
    
    
    int a = 133893;
    
    printf("Hello World %d\n", a);
    return 0;
    
    
    
    //Viene comunque compilato ed eseguito. b è caricato nel record di attivazione ma viene ignorato
//     int a = 133893;
//     float b = 7.8;
//     printf("Hello World%d\n", a, b);
//     return 0;
}
