//devo effettuare la somma arbitraria di un numero variabili di int e double

#include <stdio.h>
#include <stdarg.h>

double arbitrary_sum(double first, ...)
{
    double result = first;
    va_list ap;

    va_start(ap, first);

    while (first > 0)
    {
        result += va_arg(ap, int);
    }

    va_end(ap);

    return result;
}


int main()
{

    double res = arbitrary_sum(8,9,10.0,80.978);
    printf("risultato => %f" , res);
    return 0;
}
