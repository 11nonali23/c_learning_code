/*Lista con collegamenti al nodo successivo e al precedente*/

/*Miglioramenti al codice su suggerimento del professore
suggerimenti in fonfo al  file*/

/*NULL = pointer nullo*/

#include <stdio.h>
#include <stdlib.h>


typedef int TIPO;

struct Node
{
    TIPO val;
    struct Node* succ;
    struct Node* prev;
};

typedef struct Node NODE;

//CCreate empty node
NODE* initNode (TIPO el)
{
    NODE* tmp;
    tmp = (NODE*) malloc(sizeof(NODE));
    //controllo buona riuscita calloc
    if (tmp == NULL)
        return NULL;

    tmp->val = el;
    tmp->prev = NULL;
    tmp->succ = NULL;
    return tmp;
}


//Change a value. Insert at pos = 0 equals to insert into actual parameter node
void changeAt (NODE* n, int pos, TIPO el)
{
    if (n == NULL)
        return;

    while(pos > 0)
    {
        if (n == NULL) //position too long
            return;

        n = n->succ;

        pos--;
    }

    n->val = el;

}

//Insert new node at the bottom
void appendNode (NODE* curr, NODE* newNode)
{
    if (curr == NULL || newNode == NULL)
        return;

    while(curr->succ != NULL)
    {
        curr = curr->succ;
    }

    curr->succ = newNode;
    newNode->prev = curr;
    newNode->succ=NULL;
}

//Delete a specified node
void delNode (NODE* curr, int pos)
{
    if (curr == NULL)
        return;

    if (pos == 0)
    {
        curr->succ->prev = NULL;
        free(curr);
    }

    while(pos > 1)
    {
        if (curr == NULL) //position too long
            return;

        curr = curr->succ;

        pos--;
    }

    curr->prev->succ = curr->succ;

    curr->succ->prev = curr->prev;

    free(curr);


}

//Returns lenght
int len (NODE* n)
{
    if (n == NULL)
        return -1;

    else
    {
         int nodes = 1;
         while (n->succ != NULL)
            nodes++;

         return nodes;
     }
}

//Delete all list
void canc (NODE* n)
{
    if (n == NULL)
        return;

    while(n->succ != NULL)
    {
        n = n->succ;
        free(n->prev);
    }
}


int main()
{
    NODE* n = initNode(10);
    NODE* n2 = initNode(20);

    appendNode(n, n2);

    printf("Posizione 1 (secondo nodo): valore prima del cambio => %d", n2->val);

    changeAt(n,1,30);

    printf("Posizione 1 (secondo nodo): valore dopo il cambio => %d", n2->val);
}


/*
 1)

NODE* initNode ()
{
    NODE* tmp;
    tmp = (NODE*) malloc(sizeof(NODE));
    //controllo buona riuscita calloc
    if (tmp == NULL)
        return NULL;

    tmp->val = NULL;
    tmp->prev = NULL;
    tmp->succ = NULL;
}

 le suggerirei di modificarla come segue

NODE* initNode (TIPO el)
{
    NODE* tmp;
    tmp = (NODE*) malloc(sizeof(NODE));
    //controllo buona riuscita calloc
    if (tmp == NULL)
        return NULL;

    tmp->val = el;
    tmp->prev = NULL;
    tmp->succ = NULL;
    return tmp;
}


2)

void insertAt (NODE* n, int pos, TIPO el)
{
    if (n == NULL)
        return;

    while(pos > 0)
    {
        if (n == NULL) //position too long
            return;

        n = n->succ;

        pos--;
    }

    n->val = el;

}

cambierei nome, non effettua un inserimento ma la modifica del dato presente in posizione pos (così come ha scritto non funziona!)

void changeAt (NODE* n, int pos, TIPO el)
{
    if (n == NULL)
        return;

    while(pos > 1)
    {
        if (n == NULL) //position too long
            return;

        n = n->succ;

        pos--;
    }

    n->val = el;

}


3)

//Insert new node in specified position
void appendNode (NODE* curr, NODE* newNode)
{
    if (curr == NULL || newNode == NULL)
        return;

    while(curr->succ != NULL)
    {
        curr = curr->succ;
    }

    curr->succ = newNode;
    newNode->prev = curr;
    newNode->succ=NULL;
}

commento sbagliato o  procedura errata (nei parametri non c'è la posizione), intende l'aggiunta in coda?

4)

void delNode (NODE* curr, int pos)
{
    if (curr == NULL)
        return;

    while(pos > 0)
    {
        if (curr == NULL) //position too long
            return;

        curr = curr->succ;

        pos--;
    }

    curr->prev->succ = curr->succ;

    curr->succ->prev = curr->prev;

    free(curr);


}

Come fa a cancellare in prima posizione? Non funziona

5)

int len (NODE* n)
{
    if (n != NULL)
     {
         int nodes = 1;
         while (n->succ != NULL)
            nodes++;

         return nodes;
     }
}

la funzione non può essere chiamata su una lista vuota! guardi cosa accade se n==null, che valore viene resituito?*/
