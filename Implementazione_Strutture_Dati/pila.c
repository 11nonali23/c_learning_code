#include <stdio.h>
#include <stdlib.h>

typedef int TIPO;

struct pila
{
    TIPO* stack;
    int dimMax;
    int top;  // denota la prima posizione libera
};

typedef struct pila PILA;

//inizializzo una pila con grandezza massima n
PILA* initPila(int dim)
{
    PILA* tmp;
    tmp = (PILA*) malloc(sizeof(PILA));
    //controllo se la malloc è andata a buon fine (c' è spazio)
    if (tmp == NULL)
        return NULL;

    tmp->stack = (TIPO*) calloc(dim, sizeof(TIPO));
    //controllo se la malloc è andata a buon fine (c' è spazio)
    if (tmp->stack == NULL)
        return NULL;

    tmp->dimMax = dim;

    tmp->top = 0; //l' elemento libero è il primo

    return tmp;
}

int isEmpty(PILA* p)
{
    if(p != NULL)
        return p->top;
}

TIPO pop(PILA* p)
{
    if (!(isEmpty(p) && p != NULL))
        return p->stack[p->top--];

}

void push(PILA* p, TIPO el)
{
    if (p == NULL)
        return;

    if (p->top < p->dimMax) /* c'è posto */
    {
        p->stack[p->top] = el;
        (p->top)++;
    }
}

int top(PILA* p)
{
    if (!(isEmpty(p) && p != NULL))
        return (p->stack[p->top-1]);
}

void del(PILA* p)
{
    if (p == NULL)
        return;

    free(p);
}



int main()
{
    PILA* pila;
    pila = initPila(10);

    TIPO iter;
    for(iter = 0; iter < 5; iter++)
    {
        printf("tipo => %d", iter);
        push(pila, iter);
    }

    printf("Al top della pila c'è => %d", top(pila));

    return 0;
}
