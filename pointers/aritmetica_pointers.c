#include <stdio.h>


// In questo programma aumento l' indirizzo di pt di 40. Perchè sizeof(int) = 4 in questa macchina
int main()
{   
    
    int a = 10;
    
    int *pt = &a;

    printf("address of pt: %d\n", pt);
    printf("value of pt: %d\n\n", *pt);
    
    pt = &a + 10;   // potevo scrivere anche pt = pt + 10;
    
    printf("address of pt after a sum of 10: %d\n", pt);
    printf("value of pt after a sum of 10: %d\n", *pt);    // il valore di pt sarà totalmente diverso perchè ho cambiato indirizzo di memoria


}
