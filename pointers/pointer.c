//Uso dei puntatori

#include <stdio.h>

int main(int argc, const char * argv[])
{
    //funziona
    
     short a = 10;
     
     short *pt;
     
    pt = &a;
     
     printf("Hello World %d\n", *pt);
     
    
    
    //Con questo codice essendo pt un int leggerò i 4 byte di short come se fosse la /rappresentazione di un intero e avrò un risultato strano
     
    short a = 10;
     
     short *pt;
     
     int = &a;
     
     printf("Hello World %d\n", *pt);
     
     return 0;
}
