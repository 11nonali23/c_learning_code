#include <stdio.h>

//Mostro corrispondenza tra pointers e array nel senso che:
//un array può essere pensato come un puntatore avente come valore l' indirizzo del primo elemento
//un pointer può essere usato per accedere agli elementi di un array assegnandogli l' indirizzo di un elemento


int main(int argc, const char * argv[])
{
    unsigned int voti[20];
    unsigned int *pt;
    
    //cambio valore dell'array in posizione 9 con il pointer
    pt = &voti[9];
    *pt = 18;
    
    printf("Valore dell'array in posizione nove: %d\n\n",*pt);
    
    //assegno al pointer un array: IMPORTANTE il pointer avrà il valore di voti[0]
    pt = voti;
    
    //cambio il valore di due elementi in due modi diversi ma equivalenti
    pt[5] = 30;
    
    *(pt + 9) = 20;
    
    printf("Valore dell'array in posizione cinque: %d\n", pt[5]);
    
    printf("Valore dell'array in posizione nove: %d\n", pt[9]);


    
} 
