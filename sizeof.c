//Cosa stampa?
#include <stdio.h>

int main(int argc, const char * argv[])
{
    float a = 10; // sizeof float = 4 in questa macchina
    
    short c = 1;  // sizeof short = 2 in questa macchina
    
    int z = sizeof(a + c);
    
    printf("dimensione di a + c: %d\n", z);
    
    //SOLUZIONE
    
    //promozione di c a float. Stamperà la grandezza di float
    
    //le sizeof dipendono solo da una macchina
    
    //posso usarlo per rendere il codice portabile rendendo tutto parametrico
    
}
