#include <stdio.h>
#include <string.h>

//PROGRAMMA SCRITTO PER CAPIRE IL FUNZIONAMENTO DI STRUCT E STRUCT CON TYPEDEF

int main()
{

    //DEFINIZIONE DELLO STRUCT
    struct persona
    {
        char nome[20];
        int eta;
    };
    
    //ISTANZIAZIONE SENZA TYPE DEF
    struct persona io;
    strcpy(io.nome, "Andrea");
    io.eta = 21;
    
    //ISTANZIAZIONE CON TYPEDEF
    typedef struct persona dati_persona;
    
    dati_persona lei;
    strcpy(io.nome, "Francesca");
    io.eta = 19;
    
    return 0;

	//ALTRA ISTANZIAZIONE CON TYPEDEF
	typedef struct 
    {
        char nome[20];
        int eta;
    } altra_persona;
} 
