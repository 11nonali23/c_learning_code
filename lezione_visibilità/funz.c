#include <stdio.h>

int kk;

void prova(void) 
{
    //static int a per cambiare visibilità
    static int a;
    printf ("indirizzo di a %p\n", &a);
}


int main(int argc, const char * argv[])
{
    long long k;
    int a = 13898983;
    printf("indirizzo di kk %p\n", &kk);
    printf("indirizzo di a locale di prova %p\n", &a);
    prova();
    return 0;

}

/*L' indirizzo di kk e di à saranno ben distanti in memoria perchè uno è nello stack
e uno e nei dati globali


L'inidirizzo della variabile a di prova e quella del main invece saranno molto più
vicini perchè sono un record di attivazione opra l' altro

PROVACI
Se invece dichiarassi la a di prova static allora gli indirizzi tra le due a saranno motlo più lontani e la a statica sarà vicina a kk*/
